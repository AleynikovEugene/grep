# coding=utf-8

import argparse
import sys
import grep


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('pattern', type=str, help='the pattern to search')
    parser.add_argument('files', nargs='*', help='the files to search')
    parser.add_argument('--color', action='store_true', help='match highlight')
    parser.add_argument('-v', '--invert-match', action='store_true', help='choose not suitable strings')
    parser.add_argument('-n', '--line-number', action='store_true', help='output line number with string')
    parser.add_argument('-i', '--ignore-case', action='store_true', help='ignore registers')
    parser.add_argument('-c', '--count', action='store_true', help='output only the number of matches')
    parser.add_argument('-r', '--recursive', action='store_true', help='recursive directory search')
    parser.add_argument('-f', '--only-filename', action='store_true',
                        help='Output only the file name where the matches were found')
    parser.add_argument('-if', '--only-inverse-filename', action='store_true',
                        help='Output only the file name where no match was found')
    return parser


def start_function():
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    if not namespace.files:
        namespace.files = None
    responses = grep.search_files(namespace.files, namespace.pattern, namespace.recursive, namespace.color,
                          namespace.ignore_case, namespace.invert_match)
    for response in responses:
        grep.print_wrapper(grep.get_formed_string(response, namespace.line_number, namespace.count,
                           namespace.only_filename, namespace.only_inverse_filename))