# coding=utf-8
import glob
import os
import sys
import re
import functools
import multiprocessing

RED = '\033[91m'
END_COLOR = '\033[0m'


def search_match(pattern='', string='', color=True, ignore_case=False):
    string_is_suitable = False
    if color:
        string, is_suitable = color_is_true(pattern, string, ignore_case)
        string_is_suitable = is_suitable
    else:
        indexes = search(pattern, string, ignore_case)
        if len(indexes) > 0:
            string_is_suitable = True
    return (string, string_is_suitable)


def search(pattern='', string='', ignore_case=False):
    if ignore_case:
        pattern = pattern.lower()
        string = string.lower()
    indexes = [index.start() for index in re.finditer(pattern, string)]
    return indexes


def color_is_true(pattern='', string='', ignore_case=False):
    response = ''
    indexes = search(pattern, string, ignore_case)
    if not indexes:
        return string, False
    last_index_match_end = 0

    for index in indexes:
        response += string[last_index_match_end:index]
        response += RED + string[index:index + len(pattern)] + END_COLOR
        last_index_match_end = index + len(pattern)

    response += string[last_index_match_end:]
    return (response, True)


def print_wrapper(string=None):
    if string is not None:
        sys.stdout.write(string)


def get_formed_string(data=None, print_line_number=False, total_line_count=False, only_filename=False,
                      only_inverse_filename=False):
    if data is None:
        return None
    filename = data.keys()[0]
    if total_line_count:
        return filename + ':' + str(len(data.values()[0].keys())) + '\n'
    if only_filename:
        return filename
    elif only_inverse_filename:
        if not data[filename]:
            return filename
        else:
            return None
    else:
        response = ''
        for key in sorted(data[filename].keys()):
            value = data[filename][key]
            if not value.endswith('\n'):
                value += '\n'

            if print_line_number:
                response += str(key) + ': ' + value
            else:
                response += value
        return response


def search_file(pattern='', color=True, ignore_case=False, inverse_strings=False, filename=None):
    source = sys.stdin if filename is None else open(filename, 'r')
    line_number = 1
    response = {filename: {}}

    with source as stream:
        for line in stream.readlines():
            result_string, is_suitable = search_match(pattern, line, color, ignore_case)
            if (is_suitable and not inverse_strings) or (not is_suitable and inverse_strings):
                response[filename][line_number] = result_string
            line_number += 1
    return response


def expand_regex_in_filenames(paths=None):
    if paths is None:
        return
    response = []

    for path in paths:
        suitable_paths = glob.glob(path)
        response.extend(suitable_paths)

    return response


def get_files(paths=None, recurse=False):
    paths = expand_regex_in_filenames(paths)
    response = []

    if paths is None:
        return None

    for path in paths:
        if os.path.isfile(path):
            response.append(path)
        elif recurse:
            deep_path = [path + '/' + child for child in os.listdir(path)]
            response.extend(get_files(deep_path, recurse))
    return response


def search_files(paths=None, pattern='', recurse=False, color=True, ignore_case=False, inverse_strings=False):
    files = get_files(paths, recurse)
    response = []
    if files is not None:
        grep_func = functools.partial(search_file, pattern, color, ignore_case, inverse_strings)
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        response = pool.map(grep_func, files)
        pool.close()
        pool.join()
        return response
    else:
        response.append(search_file(pattern, color, ignore_case, inverse_strings, None))

    return response
