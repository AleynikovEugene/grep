# MyGrep
This is a python implementation of the Unix grep command.
Plan:
1. Implement a set of basic functions of the grep module. Implement parsing arguments using argparce and the ability to run the application from the command line.
2. Add the function of the grep module to functions that are executed for different arguments (recursive search, output of the number of matches, and the rest).
3. Add multithreading with the ability to specify the number of concurrent threads (each thread searches for matches for each file).
4. Add a web interface to django for the project using the grep library.
