import unittest
import random
from grep.grep import search


class SearchTest(unittest.TestCase):
    def setUp(self):
        self.line = 'hello world'
        self.pattern = 'hello'
        self.count = random.randint(1, 1000)

    def test_find_once(self):
        times = len(search(self.pattern, self.line))
        self.assertEquals(1, times)

    def test_find_many(self):
        self.line *= self.count
        times = len(search(self.pattern, self.line))
        self.assertEquals(self.count, times)

    def test_not_found(self):
        self.line = ''
        length = (search(self.pattern, self.line))
        self.assertEqual(0, len(length))

    def test_not_found_difference_of_cases_upper(self):
        self.pattern = self.pattern.upper()
        length = (search(self.pattern, self.line))
        self.assertEqual(0, len(length))

    def test_find_difference_of_cases_lower(self):
        self.pattern = self.pattern.lower()
        self.line = self.line.upper()
        index = (search(self.pattern,self.line,True))
        self.assertEqual(0, index[0])

    def test_find_difference_of_cases_upper(self):
        self.pattern = self.pattern.upper()
        index = (search(self.pattern,self.line,True))
        self.assertEqual(0,index[0])

