import os
import tempfile
import unittest
import uuid
import random
import shutil

from grep.grep import search_files


class SearchFilesTest(unittest.TestCase):
    def setUp(self):
        name = str (uuid.uuid4().int)
        self.test_dir = tempfile.mkdtemp()
        self.count = random.randint(1,1000)
        with open(os.path.join(self.test_dir, name), 'w') as f:
            f.write('hello world')
        self.test_file = os.path.join(self.test_dir, name)

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_find_file_1(self):
        find = search_files([os.path.join(self.test_file)], 'hello')[0][os.path.join(self.test_file)].keys()
        self.assertEqual(1, len(find))

    def test_find_file_many(self):
        with (open(os.path.join(self.test_file), 'w')) as f:
            f.write('hello world\n' * self.count)
        find = search_files([os.path.join(self.test_file)], 'hello')[0][os.path.join(self.test_file)].keys()
        self.assertEqual(self.count, len(find))

    def test_not_find(self):
        with (open(os.path.join(self.test_file), 'w')) as f:
            f.write('')
        find = search_files([os.path.join(self.test_file)], 'hello')[0][os.path.join(self.test_file)].keys()
        self.assertEqual(0, len(find))

    def test_wrong_path(self):
        find = search_files(['wrong_path'], 'hello')
        self.assertEqual(0, len(find))