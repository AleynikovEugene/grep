from setuptools import setup, find_packages
from os.path import join, dirname
import grep

setup(
    name="mygrep",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), "README.md")).read(),
    test_suite='tests',
    entry_points={
        "console_scripts": ['mygrep = grep.core:start_function']
    }
)