# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-29 09:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Search',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('puttern', models.CharField(max_length=200)),
                ('paths', models.CharField(max_length=200)),
                ('color', models.BooleanField(default=False)),
                ('invert_match', models.BooleanField(default=False)),
                ('line_number', models.BooleanField(default=False)),
                ('ignore_case', models.BooleanField(default=False)),
                ('only_count', models.BooleanField(default=False)),
                ('recursive', models.BooleanField(default=False)),
                ('only_filename', models.BooleanField(default=False)),
                ('only_inverse_file', models.BooleanField(default=False)),
                ('response', models.TextField()),
            ],
        ),
    ]
