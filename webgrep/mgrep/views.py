# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView
from .forms import SearchForm
from .models import Search
from grep import grep


class index(TemplateView):
    def get(self, request, *args, **kwargs):
        form = SearchForm()
        return render(request, 'mgrep/index.html', {'form': form, 'all_search': Search.objects.all()})

    def post(self, request, *args, **kwargs):
        form = SearchForm(request.POST)
        if form.is_valid():
            search = form.save(commit=False)
            responses = grep.search_files(search.paths.split(), search.puttern, search.recursive, search.color,
                                          search.ignore_case, search.invert_match)
            for response in responses:
                search.response += grep.get_formed_string(response, search.line_number, search.only_count,
                                                          search.only_filename, search.only_inverse_file)
            search.save()
        return render(request, 'mgrep/index.html', {'all_search': Search.objects.all(), 'form':form})