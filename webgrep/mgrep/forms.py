from django import forms

from .models import Search


class SearchForm(forms.ModelForm):
    class Meta:
        model = Search
        fields = ('puttern', 'paths', 'color', 'invert_match', 'line_number', 'ignore_case', 'only_count', 'recursive',
                  'only_filename', 'only_inverse_file', )