# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Search(models.Model):
    puttern = models.CharField(max_length=200)
    paths = models.CharField(max_length=200)
    color = models.BooleanField(default=False)
    invert_match = models.BooleanField(default=False)
    line_number = models.BooleanField(default=False)
    ignore_case = models.BooleanField(default=False)
    only_count = models.BooleanField(default=False)
    recursive = models.BooleanField(default=False)
    only_filename = models.BooleanField(default=False)
    only_inverse_file = models.BooleanField(default=False)
    response = models.TextField()
